package domain;


public enum Category {

	Graphic_cards,
	Mainboards,
	Hard_disks,
	Memories
}
