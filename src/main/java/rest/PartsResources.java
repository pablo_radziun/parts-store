package rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.Category;
import domain.Comment;
import domain.Part;

@Path("/parts")
@Stateless
public class PartsResources {

	@PersistenceContext
	EntityManager em;
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public List<Part> getAll()
	{
		return em.createNamedQuery("part.all", Part.class).getResultList();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public Response Add(Part part){
		em.persist(part);
		return Response.ok(part.getId()).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Response get(@PathParam("id") int id){
		Part result = em.createNamedQuery("part.id", Part.class)
				.setParameter("partId", id)
				.getSingleResult();
		if(result==null){
			return Response.status(404).build();
		}
		return Response.ok(result).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_XML)
	public Response update(@PathParam("id") int id, Part p){
		Part result = em.createNamedQuery("part.id", Part.class)
				.setParameter("partId", id)
				.getSingleResult();
		if(result==null)
			return Response.status(404).build();
		result.setPrice(p.getPrice());
		result.setName(p.getName());
		result.setCategory(p.getCategory());
		em.persist(result);
		return Response.ok().build();
	}
	
	@POST
	@Path("/{id}/comments")
	@Consumes(MediaType.APPLICATION_XML)
	public Response addComment(@PathParam("id") int partId, Comment comment){
		Part result = em.createNamedQuery("part.id", Part.class)
				.setParameter("partId", partId)
				.getSingleResult();
		if(result==null)
			return Response.status(404).build();
		result.getComments().add(comment);
		comment.setPart(result);
		em.persist(comment);
		return Response.ok().build();
	}
	
	@GET
	@Path("/{partId}/comments")
	@Produces(MediaType.APPLICATION_XML)
	public List<Comment> getComments(@PathParam("partId") int partId){
		Part result = em.createNamedQuery("part.id", Part.class)
				.setParameter("partId", partId)
				.getSingleResult();
		if(result==null)
			return null;
		return result.getComments();
	}
	
	 @DELETE
	    @Path("/{id}/comments/{commentId}")
	    public Response deleteComment(@PathParam("id") int id,
	    		@PathParam("commentId") int commentId) {
	        Part result = em.createNamedQuery("part.id", Part.class)
	                    .setParameter("partId", id)
	                    .getSingleResult();
	        if(result==null)
				return Response.status(404).build();
	        List<Comment> comments = result.getComments();
	        for (int i = 0; i < comments.size(); i++) {
	            if (comments.get(i).getId() == (commentId)) {
	                comments.get(i).setPart(null);
	                comments.remove(i);
	            }
	        }
	        em.merge(result);

	        return Response.ok().build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") int id){
		Part result = em.createNamedQuery("part.id", Part.class)
				.setParameter("part.id", id)
				.getSingleResult();
		if(result==null)
			return Response.status(404).build();
		em.remove(result);
		return Response.ok().build();
	}
}
