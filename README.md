# **partsStore** #
## **Categories of parts** ##
1. Graphic_cards
2. Mainboards
3. Hard_disks
4. Memories
## **API** ##

### Parts list ###
GET http://localhost:8080/partsStore/rest/parts

### Preview of a part using part_id ###
GET http://localhost:8080/partsStore/rest/parts/{part_id}


### Preview of comments regarding a part using part_id ###
GET http://localhost:8080/partsStore/rest/parts/{part id}/comments  

### Part adding ###
POST http://localhost:8080/partsStore/rest/parts  
XML example:
```
<part>
    <price>149,00</price>
    <name>Corsair 8GB 2400MHz CL14 Vengeance LPX</name>
    <category>Memories</category>
</part>
```
### Comment adding ###
POST http://localhost:8080/partsStore/rest/parts/{part_id}/comments  
XML example:
```
<comment>
    <content>Very, very, very... fine.</content>
</comment>
```
### Part update using part_id ###
PUT http://localhost:8080/partsStore/rest/parts/{part_id}  
XML example:

```
<part>
    <price>125,00</price>
    <name>Crucial 8GB 2133MHz CL15</name>
    <category>Memories</category>
</part>
```


### Comment deleting using part_id and comment_id ###
DELETE http://localhost:8080/partsStore/rest/parts/{part_id}/comments/{comment_id}  

### Part deleting using part_id ###
DELETE http://localhost:8080/partsStore/rest/parts/{part_id}